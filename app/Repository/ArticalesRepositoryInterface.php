<?php


namespace App\Repository;

use Illuminate\Support\Collection;
		  
interface ArticalesRepositoryInterface
{
    public function allArticales(): Collection;
    public function update($id,array $attr);
    public function destroy($id);
}
