<?php


namespace App\Repository;

use Illuminate\Support\Collection;
		  
interface SettingRepositoryInterface
{
    public function allSettings(): Collection;
    public function update(array $attr);
}
