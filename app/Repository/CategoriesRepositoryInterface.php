<?php


namespace App\Repository;

use Illuminate\Support\Collection;
		  
interface CategoriesRepositoryInterface
{

    public function sub($category_id);	
    public function subsub($subcategory_id);	
    public function allSubCategories(): Collection;
    public function allSubSubCategories(): Collection;
    public function allMainCategories(): Collection;
    public function update($id,array $attr);
    public function destroy($id);
}
