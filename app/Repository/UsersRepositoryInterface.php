<?php

namespace App\Repository;

use Illuminate\Support\Collection;

interface UsersRepositoryInterface
{
	public function allUser(): Collection;
	public function allEmployee(): Collection;
	public function allAdmin(): Collection;
	public function update($employee_id , array $attr);
	public function delete($id);
}