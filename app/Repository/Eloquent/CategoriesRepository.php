<?php

namespace App\Repository\Eloquent;

use App\Models\Category;
use App\Repository\CategoriesRepositoryInterface;
use Illuminate\Support\Collection;
                                                            
class CategoriesRepository extends BaseRepository implements CategoriesRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    protected $model;
    public function __construct(Category $model)
    {
        $this->model=$model;
    }

    /**
     * @return Collection
     */
    public function allSubCategories(): Collection
    {
        return $this->model->where('type', 2)->get();
    }

    public function sub($category_id): Collection
    {
        //dd($category_id);
        return $this->model->where('parent_id', $category_id)->get();
    } 

    public function subsub($subcategory_id): Collection
    {
        //dd($category_id);
        return $this->model->where('parent_id', $subcategory_id)->get();
    }        

    public function allSubSubCategories(): Collection
    {
        return $this->model->where('type', 3)->get();
    }    

    public function allMainCategories(): Collection
    {
        return $this->model->where('parent_id',null)->get();
    }

    public function update($id,array $attr)
    {
        return $this->model->where('id',$id)->update($attr);
    }
    public function destroy($id)
    {
        return $this->model->where('id',$id)->delete();
    }

}
