<?php

namespace App\Repository\Eloquent;

use App\Models\Artical;
use App\Repository\ArticalesRepositoryInterface;
use Illuminate\Support\Collection;
                                                            
class ArticalesRepository extends BaseRepository implements ArticalesRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    protected $model;
    public function __construct(Artical $model)
    {
        $this->model=$model;
    }

    /**
     * @return Collection
     */
    public function allArticales(): Collection
    {
        return $this->model->all();
    }

    public function update($id,array $attr)
    {
        return $this->model->where('id',$id)->update($attr);
    }
    public function destroy($id)
    {
        return $this->model->where('id',$id)->delete();
    }

}
