<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UsersRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class UsersRepository extends BaseRepository implements UsersRepositoryInterface
{

	protected $model;

	public function __construct(User $model)
	{
		$this->model = $model;
	} // end of construct

	public function allUser(): Collection
	{

		return $this->model->all();

	}

	public function allEmployee(): Collection
	{

		return $this->model->where([
			['id', '!=' , Auth::id()],
			['category_id' , Auth::user()->category_id]

		])->whereHas('permissions' ,function($q){

            $q->whereIn('name', ['add-categories','edit-categories','delete-categories']);

        })->with(['messageSender' , 'permissions'])->get();
		

	}

	public function allAdmin(): Collection
	{
		if (Auth::guard('admin')->user()) {

			$auth = Auth::guard('admin')->user();

		} else {
			$auth = Auth::user();
		}


		
		return $this->model->where([

			['military_number', '!=' , $auth->military_number]

		])->whereHas('permissions' ,function($q){

            $q->whereIn('name', ['add-categories','edit-categories','delete-categories' , 'add-data-employees' ,'edit-data-employees' ,'delete-data-employees' ,'add-orders-permissions' ,'edit-orders-permissions' ,'show-orders-permissions' , 'add-orders-vacations' ,'edit-orders-vacations' ,'show-orders-vacations' ,'change-order-status-orders-vacations' ,'add-grades' ,'edit-grades' ,'delete-grades' ,'add-degree' ,'edit-degree' ,'delete-degree' ,'add-specialties' ,'edit-specialties' ,'delete-specialties' , 'add-job-titles' ,'edit-job-titles']);

        })->with(['messageSender' , 'permissions'])->get()
;		

	}		

	public function update($employee_id , $attr)
	{
		//dd($attr);
	   return $this->model->where('id' , $employee_id)->update($attr);

	} // end of all setting

	// public function update_seen($seen_id , array $attr)
	// {
	// 	//dd($attr);
	//    return $this->model->messageSender->where('receiver_id' , $seen_id)->last()->update($attr);
	// } // end of all setting

	public function delete($id)
	{

		return $this->model->where('id',$id)->delete();

	} // end of all setting

	public function updatePassword($password){

		return $this->model->where('id',Auth::id())->update(['password'=>$password]);

	} // end of all setting

} // end of class

