<?php

namespace App\Repository\Eloquent;

use App\Models\Setting;
use App\Repository\SettingRepositoryInterface;
use Illuminate\Support\Collection;
                                    
class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    protected $model;
    public function __construct(Setting $model)
    {
        $this->model=$model;
    }

    /**
     * @return Collection
     */
    public function allSettings(): Collection
    {
        return $this->model->all();
    }

    public function update(array $attr)
    {

        foreach ($attr as $key => $att) {
                    # code...
            $this->model->where('key' , $key)->update(['value' => $att]);

        } // end of foreach  
           
        return response()->json();

    } // end of all setting    

}
