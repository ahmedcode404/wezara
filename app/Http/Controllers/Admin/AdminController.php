<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\CategoriesRepositoryInterface;
use App\Repository\ArticalesRepositoryInterface;


use App\Models\User;
class AdminController extends Controller
{

	protected $categoryrepository;
	protected $articalrepository;

	public function __construct(CategoriesRepositoryInterface $categoryrepository , ArticalesRepositoryInterface $articalrepository)
	{
		$this->categoryrepository = $categoryrepository;
		$this->articalrepository = $articalrepository;
	}

    public function index()
    {

        // messages success
        if (session('success')) {
            alert()->success('ناجح',session('success'));
        }

        $categories = $this->categoryrepository->allMainCategories();
        $subcategories = $this->categoryrepository->allSubCategories();
        $subsubcategories = $this->categoryrepository->allSubSubCategories();
        $contents = $this->articalrepository->allArticales();

    	return view('Admin.dashboard' , compact('categories' , 'subcategories' , 'subsubcategories' , 'contents'));

    } // end of index

    public function login()
    {

        // session message errors
        if (session('error')) {
            alert()->error('خطأ',session('error'));
        }    	

    	return view('Admin.login');

    } // end of loginAuth

    public function loginAuth(Request $request)
    {
    	//dd($request);
    	if (auth()->attempt(request(['email' , 'password'])) == false) {
    		return redirect()->back()->with('error', 'البريد الالكتروني او كلمه المرور غير صحيحه');
    	}

    	return redirect()->route('welcome')->with('success' , 'تم تسجيل الدخول بنجاح');

    } // end function login secretary 


    public function logout()
    {

        auth()->logout();

        return redirect()->route('login');

    } // end function logout       
}
