<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Repository\CategoriesRepositoryInterface;

class SubSubCategoryController extends Controller
{
	protected $categoryrepository;

	public function __construct(CategoriesRepositoryInterface $categoryrepository)
	{

		$this->categoryrepository = $categoryrepository;

	} // end of construct

    public function index()
    {

        // messages success
        if (session('success')) {
            alert()->success('ناجح',session('success'));
        }         

    	$subcategories = $this->categoryrepository->allSubSubCategories();

    	return view('Admin.subsubcategories.index' , compact('subcategories'));

    } // end of index

    public function create()
    {

    	$categories = $this->categoryrepository->allSubCategories();
    	return view('Admin.subsubcategories.create' , compact('categories'));

    } // end of create

    public function store(CategoryRequest $request)
    {
    	$request->validate(['parent_id' => 'required']);

    	$this->categoryrepository->create(array_merge($request->except('_token' , '_method') , ['type' => 3]));
    	return redirect()->route('subsubcategories.index')->with('success' , 'تم اضافه القسم بنجاح');

    } // end of store

    public function edit($category)
    {

    	$categories = $this->categoryrepository->allSubCategories();

    	$category = $this->categoryrepository->find($category);
    	return view('Admin.subsubcategories.edit' , compact('category' , 'categories'));

    } // end of edit

    public function update(CategoryRequest $request , $category)
    {
    	
    	$this->categoryrepository->update($category , array_merge($request->except('_token' , '_method') , ['type' => 3]));

    	return redirect()->route('subsubcategories.index')->with('success' , 'تم تعديل القسم بنجاح');
    	

    } // end of edit 

    public function destroy($category)
    {

    	$this->categoryrepository->destroy($category);

    	return redirect()->route('subsubcategories.index')->with('success' , 'تم حذف القسم بنجاح');
    	

    } // end of edit    
}
