<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ContentRequest;
use App\Repository\CategoriesRepositoryInterface;
use App\Repository\ArticalesRepositoryInterface;

class ContentController extends Controller
{

    protected $categoryrepository;
	protected $articalesrepository;

	public function __construct(CategoriesRepositoryInterface $categoryrepository , ArticalesRepositoryInterface $articalesrepository)
	{
        $this->categoryrepository = $categoryrepository;
		$this->articalesrepository = $articalesrepository;
	}


    public function index()
    {

        // messages success
        if (session('success')) {
            alert()->success('ناجح',session('success'));
        }        

        $contents = $this->articalesrepository->allArticales();

    	return view('Admin.articales.index' , compact('contents'));
    } // end of index

    public function create()
    {

    	$categories = $this->categoryrepository->allMainCategories();
    	return view('Admin.articales.create' , compact('categories'));

    } // end of create

    public function store(ContentRequest $request)
    {
        
        $attrbute = $request->except('_token' , '_method');
        $this->articalesrepository->create($attrbute);

        return redirect()->route('contents.index')->with('success' , 'تم اضافه المحتوي بنجاح');
    }

    public function edit($content)
    {

        $categories = $this->categoryrepository->allMainCategories();
        $content = $this->articalesrepository->find($content);


        return view('Admin.articales.edit' , compact('categories' , 'content'));
    }

    public function update(ContentRequest $request , $content) 
    {

        $attrbute = $request->except('_token' , '_method');

        $content = $this->articalesrepository->update($content , $attrbute);

        return redirect()->route('contents.index')->with('success' , 'تم تعديل المحتوي بنجاح');

    }

    public function destroy($content)
    {

        $this->articalesrepository->destroy($content);

        return redirect()->route('contents.index')->with('success' , 'تم حذف المحتوي بنجاح');
        

    } // end of edit    

    public function subcat(Request $request)
    {

        $subcategories = $this->categoryrepository->sub($request->category_id);

        if(isset($subcategories))
        {

        return view('Admin.articales.subcategory' , compact('subcategories'));

        }

    } // end of subcat

    public function subsubcat(Request $request)
    {

        $subsubcategories = $this->categoryrepository->subsub($request->subcategory_id);

        if($subsubcategories !== null)
        {

        return view('Admin.articales.subsubcategory' , compact('subsubcategories'));

        }

    } // end of subsubcat


}
