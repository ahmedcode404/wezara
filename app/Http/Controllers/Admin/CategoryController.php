<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use App\Repository\CategoriesRepositoryInterface;
use App\Models\Category;
class CategoryController extends Controller
{

	protected $categoryrepository;

	public function __construct(CategoriesRepositoryInterface $categoryrepository)
	{
		$this->categoryrepository = $categoryrepository;
	}

    public function index()
    {

        // messages success
        if (session('success')) {
            alert()->success('ناجح',session('success'));
        }         

    	$categories = $this->categoryrepository->allMainCategories();

    	return view('Admin.categories.index' , compact('categories'));

    } // end of index

    public function create()
    {

    	$categories = $this->categoryrepository->allMainCategories();
    	return view('Admin.categories.create' , compact('categories'));

    } // end of create

    public function store(CategoryRequest $request)
    {

    	$this->categoryrepository->create(array_merge($request->except('_token' , '_method') , ['type' => 1]));
    	return redirect()->route('categories.index')->with('success' , 'تم اضافه القسم بنجاح');

    } // end of store

    public function edit($category)
    {

    	$category = $this->categoryrepository->find($category);
    	return view('Admin.categories.edit' , compact('category'));

    } // end of edit

    public function update(CategoryRequest $request , $category)
    {

    	$this->categoryrepository->update($category , array_merge($request->except('_token' , '_method') , ['type' => 1]));

    	return redirect()->route('categories.index')->with('success' , 'تم تعديل القسم بنجاح');
    	

    } // end of edit 

    public function destroy($category)
    {

    	$this->categoryrepository->destroy($category);

    	return redirect()->route('categories.index')->with('success' , 'تم حذف القسم بنجاح');
    	

    } // end of edit               



} // end of class
