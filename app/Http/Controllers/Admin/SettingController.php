<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Classes\Helpers;
use App\Servicies\UploadFilesServices;
use Illuminate\Http\Request;
use App\Repository\SettingRepositoryInterface;
use App\Http\Requests\SettingRequest;
use App\Models\Setting;
class SettingController extends Controller
{

	protected $settingrepository;
	protected $filesServices;

	public function __construct(SettingRepositoryInterface $settingrepository , UploadFilesServices $filesServices)
	{
		$this->settingrepository = $settingrepository;
		$this->filesServices = $filesServices;
	}

	public function create()
	{

        // messages success
        if (session('success')) {
            alert()->success('ناجح',session('success'));
        } 		

		$settings = $this->settingrepository->allSettings();

    	return view('Admin.settings.edit' , compact('settings'));

	} // end of create

	public function store(SettingRequest $request)
	{


        $logo_helper=Helpers::setting('logo');

        if ($request->hasFile('logo')) {
            $img = $request->file('logo');
            $id=Helpers::setting('logo')->id;
            $logo = $this->filesServices->updateUploadfile($id, $img, 'Setting');
		}
		
		$settings = Setting::where('key' , 'logo')->first();

		$attribute = $request->except('_token' , 'logo');

        if($request->logo == null)
        {
            $attribute['logo'] = $settings->value;
        } else {

            $attribute['logo'] = $logo;

        }
		

        $this->settingrepository->update($attribute);
        
		return redirect()->back()->with('success' , 'تم التحديث بنجاح');

	} // end of edit	
}
