<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\CategoriesRepositoryInterface;
use App\Repository\ArticalesRepositoryInterface;
use App\Models\Category;

class IndexController extends Controller
{

    protected $categoryrepository;
	protected $articalesrepository;

	public function __construct(CategoriesRepositoryInterface $categoryrepository , ArticalesRepositoryInterface $articalesrepository)
	{
        $this->categoryrepository = $categoryrepository;
		$this->articalesrepository = $articalesrepository;
	}	

    public function index()
    {

        $now = \Carbon\Carbon::now();

        $date_hjri = \Alkoumi\LaravelHijriDate\Hijri::Date('Y');

        $date_milady = Date('Y');


        $categories = $this->categoryrepository->allMainCategories();
    	$contents = $this->articalesrepository->allArticales();

    	return view('Website.index' , compact('categories' , 'contents' , 'date_hjri' , 'date_milady'));

    }  // end of index

}
