<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
    public function locale($locale)
    {

    	\App::setLocale($locale);
    		
    	session()->put('locale' , $locale);

    	return redirect()->back();


    } // end of locale
}
