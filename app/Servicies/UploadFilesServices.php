<?php

namespace App\Servicies;
use App\Repository\SettingRepositoryInterface;
use App\Repository\UsersRepositoryInterface;
use Illuminate\Http\Request;
use App\Classes\FileOperations;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class UploadFilesServices
{

    protected $settingsRepository;
    protected $usersrepository;

    public function __construct(SettingRepositoryInterface $settingsRepository , UsersRepositoryInterface $usersrepository)
    {

        $this->settingsRepository=$settingsRepository;
        $this->usersrepository=$usersrepository;

    }


    public function uploadfile($img,$dir){

            $pathAfterUpload = FileOperations::StoreFileAs($dir, $img, $dir.'_img_'.Str::random(6));

        return $pathAfterUpload;
    }

    public function updateUploadfile($id, $img,$dir)
    {

        $set=$this->settingsRepository->find($id);
        if (isset($set)) {
            Storage::delete($set->value);
        }

        $set=$this->usersrepository->find($id);
        if (isset($set)) {
            Storage::delete($set->value);
        }        


            $pathAfterUpload = FileOperations::StoreFileAs($dir, $img, $dir.'_img_'.Str::random(10));

        return $pathAfterUpload;

    }

    // public function deleteUploadfile($id){
    //     $user=$this->userRepository->find($id);
    //     if (isset($user)){
    //         Storage::delete($user->image);
    //     }
    //     $slider=$this->slidersRepository->find($id);
    //     if (isset($slider)) {
    //         Storage::delete($slider->image);
    //     }

    //     $service=$this->servicesRepository->find($id);
    //     if (isset($service)) {
    //         Storage::delete($service->image);
    //     }

    //     $confirmPayment=$this->confirmPaymentRepository->find($id);
    //     if (isset($confirmPayment)) {
    //         Storage::delete($confirmPayment->file);
    //     }

    //     $opinion=$this->opinionsRepository->find($id);
    //     if (isset($opinion)) {
    //         Storage::delete($opinion->image);
    //     }
    //     return response()->json(['success'=>true]);

    // }


}
