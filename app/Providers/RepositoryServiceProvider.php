<?php

namespace App\Providers;

use App\Repository\EloquentRepositoryInterface;
use App\Repository\Eloquent\BaseRepository;
//  repository eloquent
use App\Repository\Eloquent\CategoriesRepository;
use App\Repository\Eloquent\UsersRepository;
use App\Repository\Eloquent\ArticalesRepository;
use App\Repository\Eloquent\SettingRepository;

// repository
use App\Repository\CategoriesRepositoryInterface;
use App\Repository\UsersRepositoryInterface;
use App\Repository\ArticalesRepositoryInterface;
use App\Repository\SettingRepositoryInterface;




use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app->bind(EloquentRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(CategoriesRepositoryInterface::class, CategoriesRepository::class);
        $this->app->bind(UsersRepositoryInterface::class, UsersRepository::class);
        $this->app->bind(ArticalesRepositoryInterface::class, ArticalesRepository::class);
        $this->app->bind(SettingRepositoryInterface::class, SettingRepository::class);
    
    }
}
