<?php

namespace App\Classes;


use App\Models\Setting;


class Helpers{


	static function setting($key){

		return Setting::where('key',$key)->first();

	} // end of static

} // end of class
