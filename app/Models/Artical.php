<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artical extends Model
{

	protected $table = 'articals';

  	protected $fillable = ['content_ar', 'content_en' , 'content_heddin_ar' , 'content_heddin_en' , 'category_id', 'subcategory_id' , 'subsubcategory_id'];


  	public function category()
  	{
  		return $this->belongsTo(Category::class , 'category_id' , 'id');
  	}

  	public function subcategory()
  	{
  		return $this->belongsTo(Category::class , 'subcategory_id' , 'id');
  	}

  	public function subsubcategory()
  	{
  		return $this->belongsTo(Category::class , 'subsubcategory_id' , 'id');
  	}  	  	

}
