<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	protected $table = 'categories';

  	protected $fillable = ['name_ar', 'name_en', 'type' , 'parent_id']; 

  	public function parentCategory()
  	{
  		return $this->hasMany(self::class , 'parent_id' , 'id');
	  }
	  
	  public function catContent()
	  {
		  return $this->hasMany(Artical::class , 'category_id' , 'id');
	  }
  
	  public function subCatContent()
	  {
		  return $this->hasMany(Artical::class , 'subcategory_id' , 'id');
	  }
	  
	  public function subSubCatContent()
	  {
		  return $this->hasMany(Artical::class , 'subsubcategory_id' , 'id');
	  }	  

} // end of class


