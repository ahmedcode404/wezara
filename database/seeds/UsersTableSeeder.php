<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// seed admin
        App\Models\User::create([
        	'name' => 'Admin',
        	'email' => 'info@jaadara.com',
        	'password' => bcrypt('jaadara'),
        	'role' => 'admin',
        ]); // end of app

    } // end of function 
    
} // end of class
