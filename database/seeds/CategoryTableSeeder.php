<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

  		$categories_en = ['intro' , 'Context of coaching practice' , 'Extension frameworks and theories' , 'Common problems addressed in the Counseling' , 'Extension processes and methods'];

  		$categories_ar = ['مقدمه' , 'سياق ممارسة اإلرشاد' , 'أطر ونظريات اإلرشاد' , 'المشكالت الشائعة التي يتم تناولها في اإلرشاد' , 'عمليات وطرق اإلرشاد'];

  		foreach ($categories_en as $key => $category_en) {

	    	// seed categories
	        App\Models\Category::create([

	        	'name_en' => $category_en,
	        	'name_ar' => $categories_ar[$key],
	        	'type' => 1,

	        ]); // end of app

  		} // end of foreach


  		$subcategories_en = ['Project context' , 'How to use this guide' , 'The content of this guide'];

  		$subcategories_ar = [' .سياق المشروع' , 'كيفية استخدام هذا الدليل' , 'محتوى هذا الدليل'];

  		foreach ($subcategories_en as $key => $category_en) {

	    	// seed categories
	        App\Models\Category::create([

	        	'name_en' => $category_en,
	        	'name_ar' => $subcategories_ar[$key],
	        	'type' => 2,
	        	'parent_id' => 1,

	        ]); // end of app

  		} // end of foreach


  		$subcategories_en = ['Theoretical frameworks' , 'Specific theories'];

  		$subcategories_ar = ['اُألطر النظرية' , 'نظريات محددة'];

  		foreach ($subcategories_en as $key => $category_en) {

	    	// seed categories
	        App\Models\Category::create([

	        	'name_en' => $category_en,
	        	'name_ar' => $subcategories_ar[$key],
	        	'type' => 2,
	        	'parent_id' => 3,

	        ]); // end of app

  		} // end of foreach


  		$subcategories_en = ['Problems in the family context' , 'Problems in husbands relationships' , 'Problems in the social context' , 'Personal well-being problems'];

  		$subcategories_ar = ['المشكالت في سياق األسرة' , 'المشكلات في عالقات األزواج' , 'المشكلات في السياق االجتماعي' , 'مشكلات الرفاه الشخصي'];

  		foreach ($subcategories_en as $key => $category_en) {

	    	// seed categories
	        App\Models\Category::create([

	        	'name_en' => $category_en,
	        	'name_ar' => $subcategories_ar[$key],
	        	'type' => 2,
	        	'parent_id' => 4,

	        ]); // end of app

  		} // end of foreach 


  		$subcategories_en = ['Appendix A: Reference publications in the guidance and related themes' , 'Appendix B: Status of Classification Evidence' , 'Appendix C: Diagnostic Classifications of the Diagnostic Manual
And statistician of mental disorders (“DSM5”).' , 'Appendix D: Classifications of Non-Mental Health in the Diagnostic Manual
And statistic (DSM) symbols (V)' , 'Annex E: International Classifications of Diseases (“ICD-11)
' , 'Annex F: International Classification of Diseases (“ICD-11”) Classifications
Non-mental health (Q codes)' , 'Appendix G: References' ,'Appendix H: Relevant stakeholders who were consulted'];

  		$subcategories_ar = ['الملحق أ: املنشورات املرجعية في اإلرشاد واملحاور ذات الصلة' , 'الملحق ب: حالة أدلة التصنيف' , 'الملحق ج: التصنيفات التشخيصية للدليل التشخيصي
واإلحصائي لالضطرابات النفسية )”DSM5)' , 'الملحق د: تصنيفات الصحة غير العقلية في الدليل التشخيصي
واإلحصائي )DSM )رموز )V)'  , 'الملحق هـ: تصنيفات التصنيف الدولي لألمراض )”11-ICD)' , 'الملحق و: التصنيف الدولي لألمراض )”11-ICD )”تصنيفات
الصحة غير النفسية )رموز Q)' , 'الملحق ز: املراجــع' , 'الملحق ح: أصحاب العالقة املعنيين الذي تم استشارتهم'];

  		foreach ($subcategories_en as $key => $category_en) {

	    	// seed categories
	        App\Models\Category::create([

	        	'name_en' => $category_en,
	        	'name_ar' => $subcategories_ar[$key],
	        	'type' => 2,
	        	'parent_id' => 5,

	        ]); // end of app

  		} // end of foreach 


  		$subcategories_en = ['Distinguish between counseling reference publications and mental health classifications' , 'Reference publications for guidance' , 'Mental health classification guides'];

  		$subcategories_ar = ['التمييز بين المنشورات المرجعية لإلرشاد وتصنيفات الصحة النفسية' , 'المنشورات المرجعية لإلرشاد' , 'أدلة تصنيف الصحة النفسية'];

  		foreach ($subcategories_en as $key => $category_en) {

	    	// seed categories
	        App\Models\Category::create([

	        	'name_en' => $category_en,
	        	'name_ar' => $subcategories_ar[$key],
	        	'type' => 3,
	        	'parent_id' => 15,

	        ]); // end of app

  		} // end of foreach

  		$subcategories_en = ['The purpose and role of classifications' , 'Classifications for family and social counseling' , 'Features better practice for rankings'];

  		$subcategories_ar = ['غرض ودور التصنيفات' , 'التصنيفات لإلرشاد األسري واالجتماعي' , 'ميزات ممارسة أفضل للتصنيفات'];

  		foreach ($subcategories_en as $key => $category_en) {

	    	// seed categories
	        App\Models\Category::create([

	        	'name_en' => $category_en,
	        	'name_ar' => $subcategories_ar[$key],
	        	'type' => 3,
	        	'parent_id' => 16,

	        ]); // end of app

  		} // end of foreach   		   		  		 		   		  		


    } // end of function

} // end of class
