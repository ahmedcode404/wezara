<?php

use Illuminate\Database\Seeder;

class SettingTableSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// seed logo
        App\Models\Setting::create([
        	'key' => 'logo',
        	'neckname' => 'اللوجو',
        	'type' => 'file',
        	'value' => 'logo.png',
        ]); // end of app

    	// seed name arabic
        App\Models\Setting::create([
        	'key' => 'name_ar',
        	'neckname' => 'الاسم بالعربي',
        	'type' => 'textarea',
        	'value' => 'الدليل المرجعي لأطر الإرشاد ونظرياته
ومشكلاته والطرق / الأساليب الخاصة به',
        ]); // end of app

    	// seed name english
        App\Models\Setting::create([
        	'key' => 'name_en',
        	'neckname' => 'الاسم بالانجليزي',
        	'type' => 'textarea',
        	'value' => 'A reference guide for extension frameworks and theories
His problems and his methods / methods',
        ]); // end of app

    	// seed name arabic
        App\Models\Setting::create([
        	'key' => 'about_ar',
        	'neckname' => 'المحتوي بالعربي',
        	'type' => 'textarea',
        	'value' => 'مشروع استراتيجية تطوير خدمات الإرشاد السريّ والإجتماعي ونقل المعرفة للعاملين
المملكة العربية السعودية

وزارة الموارد البشرية والتنمية الإجتماعية – وكالة التأهيل والتوجيه الإجتماعي
1441 هـ – 2020 م',
        ]); // end of app

    	// seed name english
        App\Models\Setting::create([
        	'key' => 'about_en',
        	'neckname' => 'المحتوي بالانجليزي',
        	'type' => 'textarea',
        	'value' => 'A strategy project for developing secret and social counseling services and transferring knowledge to workers
Saudi Arabia

Ministry of Human Resources and Social Development - Agency for Rehabilitation and Social Guidance',
        ]); // end of app                                

    }
}
