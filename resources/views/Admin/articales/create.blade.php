@extends('Admin.layout.app')
@section('title')
اضاقه قسم فرعي
@endsection
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">المحتوي</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('welcome')}}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('contents.index')}}">المحتوي</a>
                                    </li>
                                    <li class="breadcrumb-item active">
                                        اضف المحتوي 
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


                <!-- Input Validation start -->
                <section class="input-validation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
@include('Admin.alert')
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('contents.store') }}" novalidate>
                                            @csrf
                                            <div class="col-12">
                                                <div class="card">

                        <div class="form-group">
                            <label>اختر القسم</label>
                            <div class="controls">
                                <select class="form-control" name="category_id" id="category">
                                    <option></option>
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}" data-id="{{ $category->id }}" {{ $category->name_ar == old('category_id') ? old('category_id') : '' }}>{{ $category->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                                                                

                                                </div>
<div class="card addsuborsubsub">
</div>                                                
                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <label>المحتوي بالعربي</label>
                                                        <div class="controls">
                                                            <textarea class="form-control summernote" name="content_ar">
                                                                {{ old('content_ar') }}
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group">
                                                        <label>المحتوي بالإنجليزية</label>
                                                        <div class="controls">
                                                            <textarea class="form-control summernote" name="content_en">
                                                                {{ old('content_en') }}
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                        {{--    <div class="row">

                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group">
                                                        <label>المحتوي  الهامش بالاعربي</label>
                                                        <div class="controls">
                                                            <textarea class="form-control summernote" name="content_heddin_ar">
                                                                {{ old('content_heddin_ar') }}
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group">
                                                        <label>المحتوي  الهامش بالانجليزي</label>
                                                        <div class="controls">
                                                            <textarea class="form-control summernote" name="content_heddin_en">
                                                                {{ old('content_heddin_en') }}
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>   --}}                                                                                      

                                            <button type="submit" class="btn btn-primary">حفظ</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Input Validation end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
@section('scripts')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script>
    


    $("#category").change(function ()
    {
      var category = $("#category option:selected").click();
      var id = category.data('id');
       //alert(id);

                $.ajax({

                    type: 'GET',

                    dataType: 'html',

                    url: '{{route('subcat')}}',

                    data: {

                      category_id: id

                    }, // end of data

                    success: function(data){
                     if (data == null) {
                      
                     } else {
                        $(".addsuborsubsub").html(data);
                     }
                    }

                }); // end of ajax

    });


    $('.summernote').summernote({
            height: 400,
    });
        

</script>

@endsection
