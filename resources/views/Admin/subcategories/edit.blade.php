@extends('Admin.layout.app')
@section('title')
اضاقه قسم فرعي
@endsection
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">الاقسام الفرعيه</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('welcome')}}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('categories.index')}}">تعديل</a>
                                    </li>
                                    
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


                <!-- Input Validation start -->
                <section class="input-validation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
@include('Admin.alert')
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal" method="post" action="{{ route('subcategories.update' , $category->id) }}" novalidate>
                                            @csrf
                                            @method('PUT')
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>الاسم بالعربية</label>
                                                        <div class="controls">
                                                            <input type="text" name="name_ar" class="form-control" data-validation-required-message="هذا الحقل الزامي" placeholder="الاسم بالعربية" value="{{isset($category)?$category->name_ar:''}}">
                                                       <div class="invalid-tooltip">
                                                           @if ($errors->has('name_ar'))
                                                               <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('name_ar') }}</strong>
                            </span>
                                                           @endif
                                                       </div>
                                                        </div>
                                                    </div>



                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>الاسم بالإنجليزية</label>
                                                        <div class="controls">
                                                            <input type="text" name="name_en" class="form-control" required data-validation-required-message="هذا الحقل الزامي" placeholder="الاسم بالإنجليزية" value="{{isset($category)?$category->name_en:''}}">
                                                            <div class="invalid-tooltip">
                                                                @if ($errors->has('name_en'))
                                                                    <span class="help-block">
                               <strong style="color: red;">{{ $errors->first('name_en') }}</strong>
                            </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="">
                                                        <h4 class="card-title"  style="margin-bottom: 20px">اختر نوع القسم</h4>
                                                    </div>

                                                            <ul class="list-unstyled mb-0">
                                                             @foreach($categories as $main_category)
                                                                <li class="mr-2">
                                                                    <fieldset>
                                                                        <label>
                                                                            <input type="radio" name="parent_id" value="{{$main_category->id}}" {{isset($category) &&$category->parent_id ==$main_category->id?'checked':''}} {{isset($category)?'':'required'}}>
                                                                            {{$main_category->name_ar}}
                                                                        </label>
                                                                    </fieldset>
                                                                </li>
                                                                @endforeach

                                                            </ul>

                                                </div>
                                            </div>

                                   
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> تعديل </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Input Validation end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
