

                        @if($subcategories->count())
                        <div class="form-group">
                            <label>القسم الفرعي</label>
                            <div class="controls">
                                <select class="form-control" name="subcategory_id" id="subcategory">
                                    <option>اختر القسم الفرعي</option>
                                    @foreach($subcategories as $category)
                                    <option value="{{ $category->id }}" data-subid="{{ $category->id }}">{{ $category->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif

<script type="text/javascript">
    
    $("#subcategory").change(function ()
    {
      var category = $("#subcategory option:selected").click();
      var id = category.data('subid');
       //alert(id);

                $.ajax({

                    type: 'GET',

                    dataType: 'html',

                    url: '{{route('subsubcat')}}',

                    data: {

                      subcategory_id: id

                    }, // end of data

                    success: function(data){
                     
                      $(".addsuborsubsub").append(data);
                    }

                }); // end of ajax

    });
    
</script>            