

                        @if($subsubcategories->count())
                        <div class="form-group">
                            <label>الاقسام الاخري</label>
                            <div class="controls">
                                <select class="form-control" name="subsubcategory_id" id="subsubcategory">
                                    <option>اخبر قسم اخر</option>
                                    @foreach($subsubcategories as $category)
                                    <option value="{{ $category->id }}" data-subsubid="{{ $category->id }}">{{ $category->name_ar }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        @endif

            