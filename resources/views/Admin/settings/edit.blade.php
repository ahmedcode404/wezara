@extends('Admin.layout.app')
@section('title')
اضاقه قسم فرعي
@endsection
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">الاعدادات</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('welcome')}}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item">تحديث الاعدادات
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


                <!-- Input Validation start -->
                <section class="input-validation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
@include('Admin.alert')
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('settings.store') }}" novalidate>
                                            @csrf
                                            @foreach($settings->where('type' , 'file') as $setting)

                                            <div class="row">

                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group">
                                                        <label>{{ $setting->neckname }}</label>
                                                        <div class="controls">
                                                            <input type="{{ $setting->type }}" value="{{ $setting->value }}" name="{{ $setting->key }}" class="form-control image">
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <img src="{{ url('/storage/'. $setting->value) }}" style="width: 100px" class="img-thumbnail preview" alt="">
                                            </div>                                            

                                            @endforeach
                                            @foreach($settings->where('type' , 'textarea') as $setting)
                                            <div class="row">

                                                <div class="col-md-12">
                                                    
                                                    <div class="form-group">
                                                        <label>{{ $setting->neckname }}</label>
                                                        <div class="controls">
                                                            <textarea class="form-control summernote" name="{{ $setting->key }}">
                                                                {{ $setting->value }}
                                                            </textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                            @endforeach

                                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>تحديث البيانات</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Input Validation end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section('scripts')
<script src="{{ url('admin/summernote/summernote.min.js') }}"></script>

<script>
    
    $(document).ready(function(){ }); // end of ready

    function readURL(input) {}    

    // preview image
    $(".image").change(function() {
      readURL(this);
      if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('.preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(this.files[0]); // convert to base64 string
      }
    }); // end of preview image


    $('.summernote').summernote({
            height: 400,
    });
        

</script>

@endsection
