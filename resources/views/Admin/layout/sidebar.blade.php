
    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                            class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                            class="ficon feather icon-menu"></i></a></li>
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">{{ Auth::user()->name }}</span></div><span><img class="round" src="{{ url('Admin/app-assets/images/portrait/small/avatar-s-11.jpg') }}" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">

                                <form action="{{ route('logout') }}" method="get">
                                    <button class="dropdown-item"><i class="feather icon-power"></i> تسجيل خروج</button>
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>

<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
        <li class="nav-item mr-auto"><a class="navbar-brand" href="{{url('/')}}">
                <img src="{{ url('Website/images/main/logo.png') }}" height="100px">
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">

        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

        <li class="{{Request::segment(2) == 'welcome'?'active':''}} nav-item"><a href="{{route('welcome')}}"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Email">الرئيسيه</span></a></li>

        <li class="{{Request::segment(2) == 'categories'?'active':''}} nav-item"><a href="{{route('categories.index')}}"><i class="feather icon-folder"></i><span class="menu-title" data-i18n="Email">الاقسام</span></a></li>

        <li class="{{Request::segment(2) == 'subcategories'?'active':''}} nav-item"><a href="{{route('subcategories.index')}}"><i class="feather icon-copy"></i><span class="menu-title" data-i18n="Email">الاقسام الفرعيه</span></a></li>

        <li class="{{Request::segment(2) == 'subsubcategories'?'active':''}} nav-item"><a href="{{route('subsubcategories.index')}}"><i class="feather icon-corner-right-down"></i><span class="menu-title" data-i18n="Email">الاقسام الثانوية </span></a></li>

        <li class="{{Request::segment(2) == 'contents'?'active':''}} nav-item"><a href="{{route('contents.index')}}"><i class="feather icon-file"></i><span class="menu-title" data-i18n="Email">المحتوي</span></a></li>


        <li class="{{Request::segment(2) == 'settings'?'active':''}} nav-item"><a href="{{route('settings.create')}}"><i class="feather icon-settings"></i><span class="menu-title" data-i18n="Email">الاعدادات</span></a></li>                                

        </ul>

    </div>
</div>
<!-- END: Main Menu-->
