
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>


        <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left d-block d-md-inline-block mt-25"><a class="text-bold-800 grey darken-2" href="https://1.envato.market/pixinvent_portfolio" target="_blank"></a>جميع الحقوق محفوظه لدي موقع <b style="color: #7468f0">اذونات</b></span><span class="float-md-right d-none d-md-block">تصميم وتطوير شركة جداره للتقنيه <img src="{{ url('admin/jaadara.png') }}" width="50px"></span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->