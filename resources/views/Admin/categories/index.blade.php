@extends('Admin.layout.app')
@section('title')
الاقسام
@endsection
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">الأقسام</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('welcome')}}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item active">الأقسام
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="content-header-right text-md-right col-md-3 col-12 d-md-block d-none">
                    <div class="form-group breadcrum-right">
                        <div class="dropdown">
                            <a href="{{route('categories.create')}}" class="btn btn-primary">
                                <i class="fa fa-plus"></i>اضافه</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                <!-- Zero configuration table -->
                <section id="basic-datatable">
                    <div class="row">
                        <div class="col-12">
                            @include('Admin.alert')
                            <div class="card">
                                <div class="card-content">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table zero-configuration">
                                                <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>الإسم بالعربيه</th>
                                                    <th>نوع القسم</th>
                                                    <th>تاريخ الانشاء</th>
                                                    <th>الخيارات</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php $i=1;@endphp
                                                @foreach($categories as $category)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$category->name_ar}}</td>
                                                    <td>{{ $category->type }}</td>
                                                    <td>{{$category->created_at->format("d/m/Y")}}</td>
                                                    <td>
                                                        <a href="{{route('categories.edit',$category->id)}}" class="action-edit"><i class="feather icon-edit"></i></a>
                                                        <form action="{{route('categories.destroy',$category->id)}}" method="post" style="display: inline-block">
                                                            @csrf
                                                            @method('DELETE')
                                                            <span class="action-edit"><i class="feather icon-trash"></i></span>
                                                        </form>
                                                    </td>
                                               
                                                </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/ Zero configuration table -->



            </div>
        </div>
    </div>
    <!-- END: Content-->
@endsection

@section('dataTable')

<!-- BEGIN: Page JS-->
<script src="{{ url('admin/app-assets/js/scripts/datatables/datatable.js') }}"></script>
<!-- END: Page JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
<script src="{{ url('admin/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
<!-- END: Page Vendor JS-->


<script>
    $('#table').DataTable({});
    $('.table').DataTable({
        "language": {
//            for arabic language
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Arabic.json"
        }
    });
</script>

@endsection