@extends('Admin.layout.app')
@section('title')
اضافه قسم
@endsection
@section('content')
    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-9 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">الأقسام</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{route('welcome')}}">الرئيسية</a>
                                    </li>
                                    <li class="breadcrumb-item"><a href="{{route('categories.index')}}">الأقسام</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{isset($category)?'تعديل قسم':'أضف قسم'}}
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">


                <!-- Input Validation start -->
                <section class="input-validation">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
@include('Admin.alert')
                                <div class="card-content">
                                    <div class="card-header">
                                        <h4 class="card-title">اضافه قسم جديد</h4>
                                    </div>                                    
                                    <div class="card-body">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{ route('categories.store') }}" novalidate>
                                            @csrf

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>الاسم بالعربية</label>
                                                        <div class="controls">
                                                            <input type="text" name="name_ar" class="form-control" data-validation-required-message="هذا الحقل الزامي" placeholder="الاسم بالعربية" value="{{isset($category)?$category->name_ar:''}}">
                                                       <div class="invalid-tooltip">
                                                           @if ($errors->has('name_ar'))
                                                               <span class="help-block">
                                                                    <strong style="color: red;">{{ $errors->first('name_ar') }}</strong>
                                                                </span>
                                                           @endif
                                                       </div>
                                                        </div>
                                                    </div>



                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>الاسم بالإنجليزية</label>
                                                        <div class="controls">
                                                            <input type="text" name="name_en" class="form-control" required data-validation-required-message="هذا الحقل الزامي" placeholder="الاسم بالإنجليزية" value="{{isset($category)?$category->name_en:''}}">
                                                            <div class="invalid-tooltip">
                                                                @if ($errors->has('name_en'))
                                                                    <span class="help-block">
                                                                        <strong style="color: red;">{{ $errors->first('name_en') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">حفظ</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Input Validation end -->

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
