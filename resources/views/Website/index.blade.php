@extends('Website.layout.app')


@section('content')


        <!-- start main-header
         ================ -->
        <section class="main-header margin-div text-center wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1>
                            @if(app()->getLocale() == 'ar')
                            @foreach($settings->where('key' , 'name_ar') as $setting)
                                {!! $setting->value !!}
                            @endforeach
                            @else
                            @foreach($settings->where('key' , 'name_en') as $setting)
                                {!! $setting->value !!}
                            @endforeach
                            @endif
                        </h1>
                        <div>
                            @if(app()->getLocale() == 'ar')
                            @foreach($settings->where('key' , 'about_ar') as $setting)
                                {!! $setting->value !!}
                            @endforeach
                            @else
                            @foreach($settings->where('key' , 'about_en') as $setting)
                                {!! $setting->value !!}
                            @endforeach
                            @endif                            
                        </div>


                        <div class="header-time">
                            <span>{{ $date_hjri }} – {{ $date_milady }} م</span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end  main-header-->


        <!-- start main-section
         ================ -->
        <section class="main-section margin-div gray-bg  wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-12 main-list first_color">
                        <h3 class="first_color bold-text">
                            @awt('المحتوي')
                        </h3>
                        <ol>
                        	@foreach($categories as $key=>$category)
                            <li>
                                <a href="#mainscroll-1{{ $category->id }}" class="scroll" data-catscroll="{{ $category->id }}">
                                    @if(app()->getLocale() == 'ar')
                                    {{ $category->name_ar }} 
                                    @else
                                    {{ $category->name_en }}
                                    @endif
                                    <span class="pg-num">{{ $key + 1 }}</span>
                                </a>
                                <ul class="catsub">
                                	@if(isset($category->parentCategory))
                                	@foreach($category->parentCategory as $key2=>$subcategory)
                                    <li class="parent" data-parent="{{ $subcategory->id }}">
                                        <a href="#secondaryscroll2-1{{ $subcategory->id }}" class="subscroll" data-subscroll="{{ $subcategory->id }}">
                                           {{-- {{ $key + 1 }}.{{ $key2 + 1 }} --}}
                                           @if(app()->getLocale() == 'ar')
                                            {{ $subcategory->name_ar }} 
                                           @else
                                           {{ $subcategory->name_en }}
                                           @endif  
                                            <span class="pg-num">{{ $key + 1 }}.{{ $key2 + 1 }}</span>
                                        </a>
                                    @if($subcategory->parentCategory)
                                    @foreach($subcategory->parentCategory as $key3=>$subsubcategory)
                                    <ul class="subcat subsub" data-sub="{{ $subsubcategory->parent_id }}">

                                        <li>
                                            <a href="#secondaryscroll2-2{{ $subsubcategory->id }}">
                                                
                                                @if(app()->getLocale() == 'ar')
                                                {{ $subsubcategory->name_ar }}
                                                @else
                                                {{ $subsubcategory->name_en }}
                                                @endif

                                                 <span class="pg-num">{{ $key2 + 1 }}.{{ $key3 + 1 }}</span>
                                            </a>
                                        </li>

                                    </ul>                                    
                                    @endforeach
                                    @endif
                                    @endforeach
                                    @endif                                        
                                    </li>

                                </ul>
                            </li>
                            @endforeach


                                </ul>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--end  main-section-->



        <!-- start main-content
         ================ -->
        <section class="main-content margin-div">
            <div class="container">
                <div class="row">

                    <div class="col-12">
                        <div class="sec-content">

                            <div class="main-repeated">
                                @foreach($categories as $key=>$category)
                                <h2 id="mainscroll-1{{ $category->id }}" class="sec-main-title">{{ $key + 1 }} .
                                    @if(app()->getLocale() == 'ar') 
                                    
                                    {{ $category->name_ar }}

                                    @else                                      

                                    {{ $category->name_en }}

                                    @endif
                                </h2>
                                
                            
                            @if ($category->catContent)
                                
                            
                            @foreach($category->catContent as $key=>$content)
                                <h2 id="secondaryscroll2-1{{ $content->subcategory ? $content->subcategory->id : '' }}">
                                    @if($content->subcategory)

                                    @if(app()->getLocale() == 'ar')                                    

                                    {{ $content->subcategory->name_ar }}

                                    @else

                                    {{ $content->subcategory->name_en }}

                                    @endif

                                    @endif
                                </h2>

                                <h3 id="secondaryscroll2-2{{ $content->subsubcategory ? $content->subsubcategory->id : ''  }}" class="secondary-title">
                                    @if($content->subsubcategory)

                                    @if(app()->getLocale() == 'ar') 

                                    {{ $content->subsubcategory->name_ar }}

                                    @else

                                    {{ $content->subsubcategory->name_en }}

                                    @endif

                                    @endif
                                </h3>



                                    @if(app()->getLocale() == 'ar') 

                                    {!! $content->content_ar !!}

                                    @else

                                    {!! $content->content_en !!}

                                    @endif                                
                            @endforeach
                            @endif
                            @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end  main-content-->         

<a href="#" id="up-btn" class="auto-icon wow bounce" data-wow-duration="3s" data-wow-iteration="infinite"><i class="fa fa-long-arrow-alt-up"></i></a>


@endsection


@section('scripts')

	<script>


       // $(document).ready(function () {

           // // scroll category
           // var catscroll = $('.scroll').data('catscroll');
           // $('#mainscroll-1').attr('id' , 'mainscroll-1' + catscroll);

           // // scroll sub category
           // var subscroll = $('.subscroll').data('subscroll');
           // $('#secondaryscroll2-1').attr('id' , 'secondaryscroll2-1' + subscroll);

           // // scroll sub sub category
           // var catscroll = $('.scroll').data('catscroll');
           // $('#mainscroll-1').attr('id' , 'mainscroll-1' + catscroll);                      


       // });



        // $("#searchbox").keyup(function (e) {

        //     e.preventDefault();
        //     var searchexp = document.getElementById('searchbox').value;
        //     //alert(searchexp);

        //         $('div:contains('+ searchexp +')').css('background-color','red');
        //         //$(searchexp).css('background-color','red');

        // }); 
		

	</script>

@endsection