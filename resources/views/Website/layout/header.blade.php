


        <div class="fixed-btns auto-icon">
            <a href="#main-page" class="fixed-icon" title="الصفحة الرئيسية"><i class="fa fa-bars"></i></a>
            <a href="#search-sec" class="fixed-icon open-search" title="بحث"><i class="fa fa-search"></i></a>
        </div>


        <header id="search-sec" class="wow fadeIn">
            <div class="container">
                <div class="row align-items-center">
                    <!--start logo-grid-->
                    <div class="col-xl-3 col-lg-3 col-sm-4 col-8 logo-grid">
                        @foreach($settings->where('key' , 'logo') as $setting)
                        <a href="index.html"><img src="{{ url('/storage/' . $setting->value) }}" alt="logo" /></a>
                        @endforeach
                    </div>
                    <!--end logo-grid-->


                    <!--start left-header-grid-->
                    <div class="col-xl-9 col-lg-9
                     col-sm-8 col-4 left-header-grid text-left-dir">

                        <div class="search auto-icon">
                            <span class="search-icon open-search2 first_color">
                                <i class="fa fa-search"></i>
                            </span>

                            <div class="main-search">
                                <form action="">
                                    <div class="form-group">
                                        <input type="search" class="form-control" id="search-input" placeholder="@awt('Search')" required />
                                        <button type="button" id="search-btn"><i class="fa fa-search"></i></button>
                                    </div>


                                </form>

                            </div>
                        </div>
                        {{--@if(app()->getLocale() == 'ar')
                        <a href="locale/en" class="language first_color second_hover">En</a>
                        @else
                        <a href="locale/ar" class="language first_color second_hover">Ar</a>
                        @endif--}}

                        <img src="{{ url('Website/images/main/ro2ya.png') }}" class="ro2ya" alt="ro2ya" />


                    </div>
                    <!--end left-header-grid-->
                </div>
            </div>
        </header>
        