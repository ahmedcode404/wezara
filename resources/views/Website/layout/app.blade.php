<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>
    <title>وزارة الموارد البشرية والتنمية الاجتماعية</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#124e23">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#124e23">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#124e23">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ url('Website/images/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ url('Website/images/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ url('Website/images/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ url('Website/images/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ url('Website/images/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ url('Website/images/favicon/apple-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ url('Website/images/favicon/apple-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ url('Website/') }}images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('Website/images/favicon/apple-icon-180x180.png') }}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{ url('Website/images/favicon/android-icon-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('Website/images/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ url('Website/images/favicon/favicon-96x96.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('Website/images/favicon/favicon-16x16.png') }}">


    <!-- Style sheet
         ================ -->

    <link rel="stylesheet" href="{{ url('Website/css/bootstrap.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('Website/css/keyframes.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('Website/css/general.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('Website/css/header.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('Website/css/footer.css') }}" type="text/css" />
    <!--for arabic-->
    <link rel="stylesheet" href="{{ url('Website/css/ar.css') }}" type="text/css" />
    <!--for english-->
    <!-- <link rel="stylesheet" href="css/en.css" type="text/css" /> -->
    <link rel="stylesheet" href="{{ url('Website/css/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('Website/css/responsive.css') }}" type="text/css" />
</head>

<body>

    <div class="main-wrapper">


        <!-- start header -->

        @include('Website.layout.header')         

        <!--end header-->

        <!-- start content -->
        @yield('content')
        <!-- end content -->


    </div>


    <!-- scripts
         ================ -->
    <script type="text/javascript" src="{{ url('Website/js') }}/html5shiv.min.js"></script>
    <script type="text/javascript" src="{{ url('Website/js/respond.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('Website/js/jquery-3.4.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('Website/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('Website/js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="{{ url('Website/js/custom.js') }}" type="text/javascript"></script>

@if(app()->getLocale() == 'ar')
    <script>

        $("#search-btn").click(function(e){
            e.preventDefault();
                var text = $("#search-input").val();
                var query = new RegExp("([^ء-ي]" + text + "[^ء-ي])", "gim");
                var e_text = $(".main-content").html();
                var enew = e_text.replace(/(<span class="highlight">|<\/span>)/igm, "");
                $(".main-content").html(enew);
                var newe = enew.replace(query, "<span class='highlight'>$1</span>");
                $(".main-content").html(newe);
                $("html, body").animate({ scrollTop: $(".highlight").offset().top }, 100);
        });


$('.main-search form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                var text = $("#search-input").val();
                var query = new RegExp("([^ء-ي]" + text + "[^ء-ي])", "gim");
                var e_text = $(".main-content").html();
                var enew = e_text.replace(/(<span class="highlight">|<\/span>)/igm, "");
                $(".main-content").html(enew);
                var newe = enew.replace(query, "<span class='highlight'>$1</span>");
                $(".main-content").html(newe);
                $("html, body").animate({ scrollTop: $(".highlight").offset().top }, 300);
            }
        });        

    </script>      

@else

    <script>
            $("#search-btn").click(function(e){
                e.preventDefault();
                var text = $("#search-input").val();
                var query =new RegExp("(\\b" + text + "\\b)", "gim");
                var e_text = $(".main-wrapper").html();
                var enew = e_text.replace(/(<span class="highlight">|<\/span>)/igm, "");
               $(".main-wrapper").html(enew);
                var newe = enew.replace(query, "<span class='highlight'>$1</span>");
                $(".main-wrapper").html(newe);
                    $("html, body").animate({ scrollTop: $(".highlight").offset().top }, 100);
            });


$('.main-search form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                var text = $("#search-input").val();
                var query =new RegExp("(\\b" + text + "\\b)", "gim");
                var e_text = $(".main-content").html();
                var enew = e_text.replace(/(<span class="highlight">|<\/span>)/igm, "");
                $(".main-content").html(enew);
                var newe = enew.replace(query, "<span class='highlight'>$1</span>");
                $(".main-content").html(newe);
                $("html, body").animate({ scrollTop: $(".highlight").offset().top }, 300);
            }
        });            

    </script>

@endif        

    @yield('scripts')
    
</body>


</html>