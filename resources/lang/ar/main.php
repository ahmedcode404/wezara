<?php

return [

    // BEGIN: translate nav bar

    'English' => 'انجليزي',
    'Arabic' => 'عربي',
    'Logout' => 'تسجيل خروج',
    'Clinic' => 'عيادة',

    // END: translate nav bar

    // BEGIN: translate side bar

    'Dashboard' => 'لوحة تحكم',
    'Secretary' => 'السكرتريه',
    'Add Secretary' => 'اضافة سكرترية',
    'All Secretary' => 'كل السكرترية',
    'Patients' => 'المرضي',
    'Add Patient' => 'اضافة مريض',
    'All Patients' => 'كل المرضي',
    'Detects' => 'كل الكشوفات',
    'Bookings' => 'الحجوزات',
    'Add Booking' => 'اضافة حجز',
    'All Bookings' => 'كل الحجوزات',


    // END: translate side bar


    // BEGIN: translate secretary

    'Secretary Name' => 'اسم السكرتيرة',
    'Email' => 'البريد الالكتروني',
    'Password' => 'كلمه المرور',
    'Confirm Password' => 'تأكيد كلمه المرور',

    // END: translate secretary


    // BEGIN: translate patient

    'Patient Name' => 'اسم المريض',
    'Age' => 'العمر',
    'Address' => 'العنوان',
    'Phone' => 'رقم التليفون',
    'Edit Patient' => 'تعديل المريض',
    'Search' => 'بحث',
    'Code Patient' => 'كود المريض ....',
    'Code' => 'كود',
    'Edit' => 'تعديل البيانات',
    'Delete' => 'حذف',
    'View' => 'عرض بيانات المريض',
    'Add Booking' => 'اضافة حجز',

    // END: translate patient

    // BEGIN: translate booking

    'Date' => 'تاريخ الحجز',
    'Type' => 'نوع الحجز',
    'Price' => 'تكلفه الكشف',
    'Add Booking For' => 'اضافة حجز ل',
    'Detect' => 'كشف',
    'Operation' => 'عملية',
    'Price Plus Booking' => 'تكلفه الاعمال الاضافيه',
    'Edit Booking' => 'تعديل الحجز',
    'Booking' => 'حجوزات  ',
    'Calender' => 'مواعيد العمل',

    // END: translate booking

    // BEGIN: translate detect

    'Diagnosing English' => 'التشخيص بالانجليزي',
    'Diagnosing Arabic' => 'التشخيص بالعربي',
    'Anitdotes Arabic' => 'الادويه بالعربي',
    'Anitdotes English' => 'الادوية بالانجليزي',
    'Anitdotes' => 'الادوية',
    'Diagnosing' => 'التشخيص',
    'Other' => 'بيانات اخري',
    'Detect For Patient' => 'كشف المريض ',
    'Rays' => 'صورة الاشعة',
    'Detect Now' => 'كشف الان',
    'All Detects' => 'كشوفات اليوم ',
    'Add Detect' => 'اضافه كشف',
    'Latest Detect For Patient' => 'اخر كشف للمريض',
    'All Detect for Patient' => 'كل كشوفات المريض',
    'clinicDoctor' => 'عيادة الدكتور محمد الشربيني',
    'Desgin And Developer Jaadara Company for Tecnology' => 'تصميم وتطوير شركه جدارة لتقنية المعلومات',
    // END: translate detect

    // BEGIN: translate messages

    'Add Is Successfully' => 'تم الاضافة بنجاح',
    'Edit Is Successfully' => 'تم التعديل بنجاح',
    'Delete Is Successfully' => 'تم الحذف بنجاح',
    'Complete Detect Is Successfully' => 'تم انتهاء الكشف بنجاح',
    'Email Or Password Is Not Found' => 'البريد الالكتروني او الباسورد غير موجود',
    'Login is Successfully' => 'تم تسجيل الدخول بنجاح',
    'Success' => 'ناجح',
    'Oops' => 'خطأ',
    'Detect Complete' => 'سجل كشوفات اليوم',
    'Pound' => 'جنيه',
    'All Detect for Patient' => 'كل الكشوفات للمريض ',

    'Welcome back, please login to your account' => 'مرحبا , من فضلك قم بتسجيل الدخول',
    'Login' => 'تسجيل الدخول',
    'All rights Reserved For Website' => 'جميع الحقوق محفوظة لدي  ',
    'Dont Find Detect' => 'لايوجد كشوفات لهذا المريض',
    'Can Not Booking In Date Old' => 'لا يمكن اضافه تاريخ قديم',


    // END: translate messages


    'Data Employes' => 'بيانات الموظف',
    'Name Employes' => 'اسم الموظف',
    'Military Number' => 'الرقم العسكري',
    'Grade' => 'الرتبه',
    'Job Number' => 'الرقم الوظيفي',
    'Category' => 'القسم التابع',
    'Degree' => 'الدرجه',
    'Job Title' => 'المسمي الوظيفي',
    'Spechial' => 'التخصص',
    'Date Expired' => 'تاريخ الانتهاء',
    'Name Person' => 'اسم شخص قريب',
    'Contact Number' => 'رقم تواصل',
    'Notes' => 'الملاحظات',
    'Add' => 'حفظ',
    'Image' => 'صورة',
    'Confirmed Password' => 'تأكيد كلمه المرور',
    'Military Number Or Password Is Invalid' => 'الرقم العسكري او كلمه المرور غير صحيحه',

    'Home' => 'الرئيسية',
    'Permissions' => 'الازونات',
    'Holiday' => 'الاجازات',    
    'Messages' => 'الرسائل',    
    'Contact Order' => 'طلب تواصل',    
    'Settings' => 'الاعدادات',    
    'Help Center' => 'مركز المساعده',    
    'Message Error Password' => 'من فضلك ادخل كلمة المرور',    
    'Message Error Idn' => 'من فضلك ادخل رقم الهويه',
    'Login Is Successfully' => 'تم تسجيل الدخول بنجاح'    






];
