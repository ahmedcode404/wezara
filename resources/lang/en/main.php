<?php

return [

    // BEGIN: translate nav bar

    'English' => 'English',
    'Arabic' => 'Arabic',
    'Logout' => 'Logout',
    'Clinic' => 'Clinic',

    // END: translate nav bar


    // BEGIN: translate side bar

    'Dashboard' => 'Dashboard',
    'Secretary' => 'Secretary',
    'Add Secretary' => 'Add Secretary',
    'All Secretary' => 'All Secretary',
    'Patients' => 'Patients',
    'Add Patient' => 'Add Patient',
    'All Patients' => 'All Patients',
    'Detect' => 'All Detects',
    'Bookings' => 'Bookings',
    'Add Booking' => 'Add Booking',
    'All Bookings' => 'All Bookings',
    'Booking' => 'Booking',
    'Calender' => 'Booking Work',
    'Can Not Booking In Date Old' => 'Can Add Not Booking In Date Old',

    // END: translate side bar

    // BEGIN: translate patient

    'Secretary Name' => 'Secretary Name',
    'Email' => 'Email',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm Password',

    'Patient Name' => 'Patient Name',
    'Age' => 'Age',
    'Address' => 'Address',
    'Edit Patient' => 'Edit Patient',
    'Search' => 'Search',
    'Code Patient' => 'Code Patient ....',
    'Code' => 'Code',
    'Edit' => 'Edit Data',
    'Delete' => 'Delete',
    'View' => 'View',
    'Add Booking' => 'Add Booking',

    // END: translate patient

        // BEGIN: translate booking

    'Date' => 'Date',
    'Type' => 'Type',
    'Price' => 'Price Detect',
    'Phone' => 'Phone',
    'Add Booking For' => 'Add Booking For',
    'Detect' => 'Detect',
    'Operation' => 'Operation',
    'Price Plus Booking' => 'Price Plus Booking',
    'Edit Booking' => 'Edit Booking',
    'clinicDoctor' => 'Clinic Doctor Mohamed Elsherbeny',
    'Desgin And Developer Jaadara Company for Tecnology' => 'Desgin And Developer Jaadara Company for Tecnology',
    // END: translate booking

        // BEGIN: translate detect

    'Diagnosing English' => 'Diagnosing English',
    'Diagnosing Arabic' => 'Diagnosing Arabic',
    'Anitdotes Arabic' => 'Anitdotes Arabic',
    'Anitdotes English' => 'Anitdotes English',
    'Anitdotes' => 'Anitdotes',
    'Other' => 'Other',
    'Diagnosing' => 'Diagnosing',
    'Detect For Patient' => 'Detect For Patient',
    'Rays' => 'Image Rays',
    'Detect Now' => 'Detect Now',
    'All Detects' => 'All Detects',
    'Add Detect' => 'Add Detects',
    'Latest Detect For Patient' => 'Latest Detect For Patient',
    'All Detect for Patient' => 'All Detect for Patient',

    // END: translate detect

    // BEGIN: translate messages

    'Add Is Successfully' => 'Add Is Successfully',
    'Edit Is Successfully' => 'Edit Is Successfully',
    'Delete Is Successfully' => 'Delete Is Successfully',
    'Complete Detect Is Successfully' => 'Complete Detect Is Successfully',
    'Email Or Password Is Not Found' => 'Email Or Password Is Not Found',
    'Login is Successfully' => 'Login is Successfully',
    'Success' => 'Success',
    'Oops' => 'Oops',
    'Detect Complete' => 'Record Detect Today',
    'Pound' => 'Pound',
    'All Detect for Patient' => 'All Detect for Patient',
    'Dont Find Detect' => 'Dont Find Detect',


    // END: translate messages

    'Welcome back, please login to your account' => 'Welcome back, please login to your account',
    'Login' => 'Login',
    'All rights Reserved For Website' => 'All rights Reserved For Website',


    // new Project Auzonat
    'Data Employee' => 'Data Employee',
    'Name Employee' => 'Name Employee',
    'Military Number' => 'Military Number',
    'Grade' => 'Grade',
    'Job Number' => 'Job Number',
    'Category' => 'Category',
    'Degree' => 'Degree',
    'Job Title' => 'Job Title',
    'Spechial' => 'Spechial',
    'Date Expired' => 'Date Expired',
    'Name Person' => 'Name Person',
    'Contact Number' => 'Contact Number',
    'Notes' => 'Notes',
    'Add' => 'Add',
    'Image' => 'Image',
    'Confirmed Password' => 'Confirmed Password',    
    'Military Number Or Password Is Invalid' => 'Military Number Or Password Is Invalid',


    'Home' => 'Home',
    'Permissions' => 'Permissions',
    'Holiday' => 'Holiday',    
    'Messages' => 'Messages',    
    'Contact Order' => 'Messages',    
    'Settings' => 'Settings',    
    'Help Center' => 'Help Center',    
    'Message Error Password' => 'Please Enter Your Password',    
    'Message Error Idn' => 'Please Enter You Idn',    
    'Login Is Successfully' => 'Login Is Successfully',  

    


];
