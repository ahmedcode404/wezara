$('.main-list ul li:has(ul)').closest("ul").addClass('cat');


$(".main-list a").click(function() {
    $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top - 20 }, 100);
});


//search
$(".open-search").click(function() {
    $("html, body").animate({ scrollTop: $($(this).attr("href")).offset().top }, 300);
    setTimeout(function() {
        $(".main-search").addClass("active")
    }, 400);

});


$(".open-search2").click(function() {
    $(".main-search").toggleClass("active");
});


$(function() {
    var $wins = $(window); // or $box parent container
    var $boxs = $(".main-search,.open-search,.open-search2");
    $wins.on("click.Bst", function(event) {
        if (
            $boxs.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxs.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".main-search").removeClass("active");

        }
    });
});


// scroll
$(document).scroll(function() {
    var header_height = $("header").outerHeight() + $(".main-header").outerHeight();

    var y = $(this).scrollTop();
    if (y > header_height) {
        $(".fixed-btns").addClass("active")
    } else {
        $(".fixed-btns").removeClass("active")
    }
});

//up btn 
$(window).scroll(function() {
    var hT = $('.main-section').offset().top,
        hH = $('.main-section').outerHeight(),
        wH = $(window).height(),
        wS = $(this).scrollTop();
    if (wS > (hT+hH-wH)){
        $("#up-btn,.fixed-btns").addClass("active");
    } else{
        $("#up-btn,.fixed-btns").removeClass("active");

    }
 });

 $(document).on('click','#up-btn', function(e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: 0
    }, 200);
});


//wow
new WOW().init();
