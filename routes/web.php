<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('login' , 'Admin\AdminController@login')->name('login');
Route::post('login/auth' , 'Admin\AdminController@loginAuth')->name('login.auth');
Route::get('logout' , 'Admin\AdminController@logout')->name('logout');

Route::prefix('admin')->middleware('auth')->group(function() {

	Route::get('welcome' , 'Admin\AdminController@index')->name('welcome');
	Route::resource('categories' , 'Admin\CategoryController');
	Route::resource('subcategories' , 'Admin\SubCategoryController');
	Route::resource('subsubcategories' , 'Admin\SubSubCategoryController');
	Route::resource('contents' , 'Admin\ContentController');
	Route::resource('settings' , 'Admin\SettingController');
	Route::get('subcat' , 'Admin\ContentController@subcat')->name('subcat');
	Route::get('subsubcat' , 'Admin\ContentController@subsubcat')->name('subsubcat');

});

// route index
Route::get('locale/{locale}' , 'Website\LocaleController@locale')->name('locale');
Route::get('/' , 'Website\IndexController@index');
